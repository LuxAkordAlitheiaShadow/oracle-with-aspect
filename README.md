# Oracle with aspect

## Introduction

This scholar project is a simulator of response with probabilities for highlight object-oriented programming's concepts.
This one is an ehanced version of Oracle project (https://gitlab.com/LuxAkordAlitheiaShadow/oracle) where we add AspectJ concepts.

There are tribes calling their Oracle. The Oracle can respond some services according to the tribe and their probabilities.

## Resources

The UML diagrams of this project is in `/Resources/UML` folder in .vpp format (Visual Paradigm), also exported in data.zip and in format `.pnj`.

All screenshot are in `/Resources/Screenshot` folder.

## Running project

This project was developed with IntelliJ with the AspectJ compiler Ajc.

You just need to run the Main.java with an IDE with AspectJ (like Eclipse or IntelliJ) or compile the project to an executable.

For use AspectJ with IntelliJ, please follow this external guide : https://tzachsolomon.blogspot.com/2015/08/how-to-create-hello-world-with-intellij.html 

## Features

### Legacy

At the beginning of the simulation, tribes are created with a hard coded probabilities.

The tribes probabilities are :

| Services   | Tribes         |            |               |               |
|------------|----------------|------------|---------------|---------------|
| Services   | Drunked Breton | Black Feet | Masked Dancer | Salted Butter |
| Random Tip | 0              | 0          | 0             | 0.4           |
| Good Tip   | 0              | 0.8        | 0.2           | 0.4           |
| Disaster   | 0.7            | 0          | 0             | 0.1           |
| Miracle    | 0.3            | 0.2        | 0             | 0.1           |

For each cycle, tribes are calling their Oracle and have probabilities to retrieve a response.
The probability is rolled from 0 to 100 and realise a Service according to tribe's probabilities

When a tribe trigger a disaster for a second time, the disaster is removed.

### AspectJ

First, we make some console print through the console, with `[TYPE] message {timestamp}` format, for making sure we are right viewing a message from an execution pointcut.

Then, all console print are write in a logs file (`Log.txt`). 

## Screenshot

Here a screenshot of the execution.

![Execution](./Resources/Screenshot/Execution.png)

And a screenshot of logs file.

![Log](./Resources/Screenshot/Log.png)