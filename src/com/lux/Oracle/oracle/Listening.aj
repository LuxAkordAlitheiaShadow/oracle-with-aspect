package com.lux.Oracle.oracle;

import com.lux.Oracle.tribe.Tribe;

import java.io.*;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public aspect Listening {
    private SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


    pointcut printDisaster(): execution(* Listener.responseDisaster(..));
    pointcut printResponseNothing(): execution(* Listener.responseNothing());
    pointcut printMiracle(): execution(* Listener.responseMiracle());
    pointcut printGoodTip(): execution(* Listener.responseGoodTip());
    pointcut printRandomTip(): execution(* Listener.responseRandomTip());
    pointcut printTribe(): execution(* Listener.listening(..));

    after() returning(String message) : printDisaster()
    {
        System.out.println("[Result] " + message);
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        System.out.println(sdf3.format(timestamp));
        String text = "[Result] " + message +", timestamp : "+timestamp+"\n";
        writeFile(text);
    }

    after() returning(String message) : printResponseNothing()
    {
        System.out.println("[Result] " + message);
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        System.out.println(sdf3.format(timestamp));
        String text = "[Result] " + message +", timestamp : "+timestamp+"\n";
        writeFile(text);
    }

    after() returning(String message) : printMiracle()
    {
        System.out.println("[Result] " + message);
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        System.out.println(sdf3.format(timestamp));
        String text = "[Result] " + message +", timestamp : "+timestamp+"\n";
        writeFile(text);
    }

    after() returning(String message) : printGoodTip()
    {
        System.out.println("[Result] " + message);
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        System.out.println(sdf3.format(timestamp));
        String text = "[Result] " + message +", timestamp : "+timestamp+"\n";
        writeFile(text);
    }

    after() returning(String message) : printRandomTip()
    {
        System.out.println("[Result] " + message);
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        System.out.println(sdf3.format(timestamp));
        String text = "[Result] " + message +", timestamp : "+timestamp+"\n";
        writeFile(text);
    }

    before() : printTribe()
    {
        System.out.println("[Listening]");
    }

    after() returning(String tribeName) : printTribe()
    {
        System.out.println("[Oracle has listen] " + tribeName);
        writeFile("[Oracle has listen] " + tribeName + "\n\n");
        System.out.println("");
    }

    private void writeFile(String message)
    {
        try {
            FileWriter fw = new FileWriter("Log.txt",true);
            fw.write(message);
            fw.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}