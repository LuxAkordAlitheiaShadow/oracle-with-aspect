package com.lux.Oracle.oracle;

import com.lux.Oracle.tribe.Tribe;

/**
 * @Package: com.lux
 * @Author: Titouan 'Lux' Allain
 * @Date: 2/14/22
 * @Time: 5:40 PM
 */
final class Disaster extends Service
{
    // Constructor.
    public Disaster()
    {

    }

    // Methods.

    // Method make or remove disaster according to the current tribe's disaster triggering
    String triggerDisasterEvent( Tribe tribe )
    {
        // Tribe isn't under disaster statement
        if ( !tribe.getUnderDisaster() )
        {
            return makeDisaster(tribe);
        }
        // Tribe is under disaster statement
        else
        {
            return removeDisaster(tribe);
        }
    }

    String makeDisaster( Tribe tribe )
    {
        tribe.setUnderDisaster(true);
        return ("It's a disaster");
    }

    String removeDisaster(Tribe tribe)
    {
        tribe.setUnderDisaster(false);
        return ("The disaster has been removed");
    }
}
