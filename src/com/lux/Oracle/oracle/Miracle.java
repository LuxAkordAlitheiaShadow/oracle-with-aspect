package com.lux.Oracle.oracle;

/**
 * @Package: com.lux
 * @Author: Titouan 'Lux' Allain
 * @Date: 2/14/22
 * @Time: 5:46 PM
 */
public final class Miracle extends Service
{
    // Constructor.
    public Miracle()
    {

    }

    // Methods.

    String makeMiracle()
    {
        return("It's a miracle !");
    }
}
