package com.lux.Oracle.oracle;

public class Oracle
{
    private static Oracle instance = new Oracle();
    private Tip tip;
    private Miracle miracle;
    private Listener listener;
    private Disaster disaster;

    // private creator
    private Oracle()
    {
        this.tip = new Tip();
        this.miracle = new Miracle();
        this.listener = new Listener();
        this.disaster = new Disaster();
    }

    public static Oracle getInstance() {
        return instance;
    }


    public Tip getTips()
    {
        return tip;
    }

    public Miracle getMiracle()
    {
        return miracle;
    }

    public Listener getListen()
    {
        return listener;
    }

    public Disaster getDisaster()
    {
        return disaster;
    }

}
