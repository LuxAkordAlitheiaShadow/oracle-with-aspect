package com.lux.Oracle.oracle;

/**
 * @Package: com.lux
 * @Author: Titouan 'Lux' Allain
 * @Date: 2/14/22
 * @Time: 5:47 PM
 */
public final class Tip extends Service
{
    // Constructor.
    public Tip()
    {

    }

    // Methods.

    String giveGoodTip()
    {
        return("It's a good tip !");
    }

    String giveRandomTip()
    {
        return("It's a random tip !");
    }
}
