package com.lux.Oracle.oracle;

import com.lux.Oracle.tribe.Probability;
import com.lux.Oracle.tribe.Tribe;

import java.util.Random;

/**
 * @Package: com.lux
 * @Author: Titouan 'Lux' Allain
 * @Date: 2/14/22
 * @Time: 5:41 PM
 */
public final class Listener extends Service implements ListenerInterface
{
    // Constructor.
    public Listener()
    {

    }

    // Methods.

    // Method for choosing according to tribe's probabilities what response to this one
    public String listening(Tribe tribe)
    {
        System.out.println("The oracle listening the " + tribe.getName() + " tribe.");
        Probability tribeProbability = tribe.getProbability();
        Random randomizer = new Random();
        int rand = randomizer.nextInt(100); // Rand an integer 0 to 99
        System.out.println("Roll value : " + rand);

        // Oracle response a random tip statement
        if ( rand < tribeProbability.getRandomTipProbability() )
        {
            this.responseRandomTip();
        }
        // Oracle response a good tip statement
        else if ( rand < tribeProbability.getRandomTipProbability() + tribeProbability.getGoodTipProbability() )
        {
            this.responseGoodTip();
        }
        // Oracle response a disaster statement
        else if ( rand < tribeProbability.getDisasterProbability() + tribeProbability.getGoodTipProbability() + tribeProbability.getRandomTipProbability() )
        {
            this.responseDisaster( tribe );
        }
        // Oracle response a miracle statement
        else if ( rand < tribeProbability.getDisasterProbability() + tribeProbability.getGoodTipProbability() + tribeProbability.getRandomTipProbability() + tribeProbability.getMiracleProbability() )
        {
            this.responseMiracle();
        }
        else
        {
            this.responseNothing();
        }
        return tribe.getName();
    }

    // Method calling a random tip
    private String responseRandomTip()
    {
        Tip tip = Oracle.getInstance().getTips();
        return tip.giveRandomTip();
    }

    // Method calling a good tip
    private String responseGoodTip()
    {
        Tip tip = Oracle.getInstance().getTips();
        return tip.giveGoodTip();
    }

    // Method calling a disaster
    private String responseDisaster(Tribe tribe)
    {
        Disaster disaster = Oracle.getInstance().getDisaster();
        return disaster.triggerDisasterEvent( tribe );
    }

    // Method calling a miracle
    private String responseMiracle()
    {
        Miracle miracle = Oracle.getInstance().getMiracle();
        return miracle.makeMiracle();
    }

    // Method for response nothing
    private String responseNothing()
    {
        return ("The oracle response nothing.");
    }
}
