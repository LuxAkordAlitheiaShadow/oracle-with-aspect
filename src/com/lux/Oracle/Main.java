package com.lux.Oracle;

import com.lux.Oracle.tribe.BlackFeet;
import com.lux.Oracle.tribe.DrunkedBreton;
import com.lux.Oracle.tribe.MaskedDancer;
import com.lux.Oracle.tribe.SaltedButter;

/**
 * @Package: com.lux
 * @Author: Titouan 'Lux' Allain
 * @Date: 2/14/22
 * @Time: 4:40 PM
 */
public class Main
{
    public static void main(String args[])
    {
        DrunkedBreton drunkedBreton = new DrunkedBreton();
        BlackFeet blackFeet = new BlackFeet();
        MaskedDancer maskedDancer = new MaskedDancer();
        SaltedButter saltedButter = new SaltedButter();

        for ( int cycle = 0 ; cycle < 10 ; cycle++ )
        {
            System.out.println("Cycle " + cycle + " :");

            drunkedBreton.callOracle();
            blackFeet.callOracle();
            maskedDancer.callOracle();
            saltedButter.callOracle();
        }
    }
}
