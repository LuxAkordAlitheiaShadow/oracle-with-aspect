package com.lux.Oracle.tribe;

public class Probability
{
    private int randomTipProbability;
    private int goodTipProbability;
    private int miracleProbability;
    private int disasterProbability;

    public int getRandomTipProbability() {
        return randomTipProbability;
    }

    public int getGoodTipProbability() {
        return goodTipProbability;
    }

    public int getMiracleProbability() {
        return miracleProbability;
    }

    public int getDisasterProbability() {
        return disasterProbability;
    }

    public void setRandomTipProbability(int randomTipProbability) {
        this.randomTipProbability = randomTipProbability;
    }

    public void setGoodTipProbability(int goodTipProbability) {
        this.goodTipProbability = goodTipProbability;
    }

    public void setMiracleProbability(int miracleProbability) {
        this.miracleProbability = miracleProbability;
    }

    public void setDisasterProbability(int disasterProbability) {
        this.disasterProbability = disasterProbability;
    }
}
