package com.lux.Oracle.tribe;

public abstract class Tribe
{
    // Attributes.
    protected String name;
    protected Probability probability;
    protected boolean underDisaster = false;

    // Getters.
    public String getName()
    {
        return this.name;
    }

    public Probability getProbability()
    {
        return this.probability;
    }

    public boolean getUnderDisaster()
    {
        return this.underDisaster;
    }

    // Setter.
    public void setUnderDisaster(boolean newState)
    {
        this.underDisaster = newState;
    }
}
