package com.lux.Oracle.tribe;

import com.lux.Oracle.oracle.Oracle;

public final class SaltedButter extends Tribe
{
    private Oracle salt;

    public SaltedButter()
    {
        name = "SaltedButter";
        this.salt = Oracle.getInstance();
        //Initialisation of probabilities
        this.probability = new Probability();
        this.probability.setRandomTipProbability(40);
        this.probability.setGoodTipProbability(40);
        this.probability.setDisasterProbability(10);
        this.probability.setMiracleProbability(10);
    }

    public void callOracle()
    {
        salt.getListen().listening(this);
    }
}
