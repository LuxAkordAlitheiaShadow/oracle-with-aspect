package com.lux.Oracle.tribe;

import com.lux.Oracle.oracle.Oracle;

public final class BlackFeet extends Tribe
{
    private Oracle nightSpirit;

    public BlackFeet()
    {
        name = "BlackFeet";
        this.nightSpirit = Oracle.getInstance();
        //Initialisation of probabilities
        this.probability = new Probability();
        this.probability.setRandomTipProbability(0);
        this.probability.setGoodTipProbability(80);
        this.probability.setDisasterProbability(0);
        this.probability.setMiracleProbability(20);
    }

    public void callOracle()
    {
        nightSpirit.getListen().listening(this);
    }
}
